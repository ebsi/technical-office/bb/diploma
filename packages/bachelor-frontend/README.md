# Diploma Bachelor Frontend

## Requirements

You need:

- Node.js >= 12
- Yarn >= 1.22.0

## Getting started

Install the dependencies:

```sh
yarn
```

Start the dev server:

```sh
yarn start
```

Create a production build:

```sh
yarn build
```

## Auditing the dependencies

Audit dependencies with:

```sh
yarn audit
```

## Linting

Lint all the files with one command:

```sh
yarn lint
```

You can also run the linters one by one:

### ESLint

```sh
yarn lint:js
```

### stylelint

```sh
yarn lint:css
```

### Prettier

```sh
yarn lint:prettier
```

## Testing

```sh
yarn test
```
