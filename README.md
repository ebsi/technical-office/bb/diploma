![EBSI Logo](https://ec.europa.eu/cefdigital/wiki/images/logo/default-space-logo.svg)

# Diploma

## Table of Contents

1. [Getting started](#Getting-started)
2. [Linting](#Linting)
3. [Auditing](#Auditing)
4. [Testing](#Testing)
5. [Run with Docker Compose](#Run-with-Docker-Compose)

## Getting started

### Requirements

You need:

- Node.js >= 12
- Yarn >= 1.22.0

### Installation

```sh
yarn install
```

## Linting

```sh
yarn lint
```

## Auditing

```sh
yarn run audit
```

## Testing

First of all, make sure to correctly configure your environment. If you rely on `.env` files, make sure to create the proper `.env` files in the different sub-packages: `packages/bachelor-frontend`, `packages/backend` and `packages/master-frontend`. In each folder, you can find a file named `.env.example` from which you can create a copy.

Note that the private keys must be base64-encoded.

Now you can run the following command to run all the tests:

```sh
yarn test
```

## Run with Docker Compose

Before starting Docker Compose, create a copy of `.env.example` and name it `.env`. Set the environment variables accordingly. From now on, we'll consider that you're using the default environement variables.

Now, run:

```sh
docker-compose up --build
```

And open http://localhost:8080/demo/diploma/bachelor or http://localhost:8080/demo/diploma/master to see the results.

## Licensing

Copyright (c) 2019 European Commission
Licensed under the EUPL, Version 1.2 or - as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
You may not use this work except in compliance with the Licence.
You may obtain a copy of the Licence at:

- <https://joinup.ec.europa.eu/page/eupl-text-11-12>

Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the Licence for the specific language governing permissions and limitations under the Licence.
